package com.mag.magasin.controller;


import com.mag.magasin.entity.Employee;
import com.mag.magasin.service.EmployeeService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
@RequestMapping("api/users/employees")
@RestController
public class EmployeeController {

    private EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("")
    public Iterable<Employee> getEmployee() {
        return employeeService.getEmployee();
    }

    @GetMapping("/{id}")
    public Optional<Employee> getEmployee(@PathVariable Long id) {
        return employeeService.getOneEmployee(id);
    }

    @DeleteMapping("/{id}")
    public void deleteEmployeeByID(@PathVariable Long id) {
        employeeService.deleteEmployeeByID(id);
    }

    @PostMapping("")
    private Employee saveEmployee(@RequestBody Employee employee) {
        return employeeService.save(employee);
    }

    @PutMapping("/{id}")
    private Employee updateEmployee(@PathVariable Long id, @RequestBody Employee employee) {
        return employeeService.updateEmployee(id, employee);
    }

}
