package com.mag.magasin.controller;

import com.mag.magasin.entity.Figurine;
import com.mag.magasin.service.FigurineService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/product/figurines")
@RestController
public class FigurineController {

    private FigurineService figurineservice;

    public FigurineController(FigurineService figurineservice) {
        this.figurineservice = figurineservice;
    }

    @GetMapping("")
    public Iterable<Figurine> getFigurine() {
        return figurineservice.getFigurine();
    }

    @GetMapping("/{id}")
    public Optional<Figurine> getFigurine(@PathVariable Long id) {
        return figurineservice.getOneFigurine(id);
    }

    @DeleteMapping("/{id}")
    public void deleteFigurineByID(@PathVariable Long id) {
        figurineservice.deleteFigurineByID(id);
    }

    @PostMapping("")
    private Figurine saveFigurine(@RequestBody Figurine figurine) {
        return figurineservice.save(figurine);
    }

    @PutMapping("/{id}")
    private Figurine updateFigurine(@PathVariable Long id, @RequestBody Figurine figurine) {
        return figurineservice.updateFigurine(id, figurine);
    }
}
