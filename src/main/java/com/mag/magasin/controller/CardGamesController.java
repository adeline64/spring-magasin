package com.mag.magasin.controller;

import com.mag.magasin.entity.CardGames;
import com.mag.magasin.service.CardGamesservice;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/product/CardGames")
@RestController
public class CardGamesController {

    private CardGamesservice cardGamesservice;

    public CardGamesController(CardGamesservice cardGamesservice) {
        this.cardGamesservice = cardGamesservice;
    }

    @GetMapping("")
    public Iterable<CardGames> getCardGames() {
        return cardGamesservice.getCardGames();
    }

    @GetMapping("/{id}")
    public Optional<CardGames> getCardGames(@PathVariable Long id) {
        return cardGamesservice.getOneCardGames(id);
    }

    @DeleteMapping("/{id}")
    public void deleteCardGamesByID(@PathVariable Long id) {
        cardGamesservice.deleteCardGamesByID(id);
    }

    @PostMapping("")
    private CardGames saveCardGames(@RequestBody CardGames cardGames) {
        return cardGamesservice.saveCardGames(cardGames);
    }

    @PutMapping("/{id}")
    private CardGames updateBoardGames(@PathVariable Long id, @RequestBody CardGames cardGames) {
        return cardGamesservice.updateCardGames(id, cardGames);
    }
}
