package com.mag.magasin.controller;

import com.mag.magasin.entity.Customer;
import com.mag.magasin.service.CustomerService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/users/customers")
@RestController
public class CustomerController {

    private CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("")
    public Iterable<Customer> getCustomer() {
        return customerService.getCustomer();
    }

    @GetMapping("/{id}")
    public Optional<Customer> getCustomer(@PathVariable Long id) {
        return customerService.getOneCustomer(id);
    }

    @DeleteMapping("/{id}")
    public void deleteCustomerByID(@PathVariable Long id) {
        customerService.deleteCustomerByID(id);
    }

    @PostMapping("")
    private Customer saveCustomer(@RequestBody Customer customer) {
        return customerService.save(customer);
    }

    @PutMapping("/{id}")
    private Customer updateCustomer(@PathVariable Long id, @RequestBody Customer customer) {
        return customerService.updateCustomer(id, customer);
    }

}
