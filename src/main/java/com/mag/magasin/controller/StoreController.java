package com.mag.magasin.controller;

import com.mag.magasin.entity.Store;
import com.mag.magasin.service.StoreService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/store")
@RestController
public class StoreController {

    private StoreService storeservice;

    public StoreController(StoreService storeservice) {
        this.storeservice = storeservice;
    }

    @GetMapping("")
    public Iterable<Store> getStore() {
        return storeservice.getStore();
    }

    @GetMapping("/{id}")
    public Optional<Store> getStore(@PathVariable Long id) {
        return storeservice.getOneStore(id);
    }

    @DeleteMapping("/{id}")
    public void deleteStoreByID(@PathVariable Long id) {
        storeservice.deleteStoreByID(id);
    }

    @PostMapping("")
    private Store saveStore(@RequestBody Store store) {
        return storeservice.save(store);
    }

    @PutMapping("/{id}")
    private Store updateStore(@PathVariable Long id, @RequestBody Store store) {
        return storeservice.updateStore(id, store);
    }
}
