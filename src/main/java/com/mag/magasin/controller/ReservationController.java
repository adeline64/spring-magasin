package com.mag.magasin.controller;

import com.mag.magasin.entity.Reservation;
import com.mag.magasin.service.ReservationService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/reservation")
@RestController
public class ReservationController {

    private ReservationService reservationService;

    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping("")
    public Iterable<Reservation> getReservation() {
        return reservationService.getReservation();
    }

    @GetMapping("/{id}")
    public Optional<Reservation> getReservation(@PathVariable Long id) {
        return reservationService.getOneReservation(id);
    }

    @DeleteMapping("/{id}")
    public void deleteReservationByID(@PathVariable Long id) {
        reservationService.deleteReservationByID(id);
    }

    @PostMapping("")
    private Reservation saveReservation(@RequestBody Reservation reservation) {
        return reservationService.saveReservation(reservation);
    }

    @PutMapping("/{id}")
    private Reservation updateReservation(@PathVariable Long id, @RequestBody Reservation reservation) {
        return reservationService.updateReservation(id, reservation);
    }
}
