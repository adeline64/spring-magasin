package com.mag.magasin.controller;

import com.mag.magasin.entity.Address;
import com.mag.magasin.service.AddressService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/address")
@RestController
public class AddressController {

        private AddressService addressService;

        public AddressController(AddressService addressService) {
            this.addressService = addressService;
        }

        @GetMapping("")
        public Iterable<Address> getAddress() {
            return addressService.getAddress();
        }

        @GetMapping("/{id}")
        public Optional<Address> getAddress(@PathVariable Long id) {
            return addressService.getOneAddress(id);
        }

        @DeleteMapping("/{id}")
        public void deleteAddressByID(@PathVariable Long id) {
            addressService.deleteAddressByID(id);
        }

        @PostMapping("")
        private Address saveAddress(@RequestBody Address address) {
            return addressService.save(address);
        }

        @PutMapping("/{id}")
        private Address updateAddress(@PathVariable Long id, @RequestBody Address address) {
            return addressService.updateAddress(id, address);
        }
}
