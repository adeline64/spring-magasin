package com.mag.magasin.controller;

import com.mag.magasin.entity.RolePlaying;
import com.mag.magasin.service.RolePlayingService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/product/rolePlayingGame")
@RestController
public class RolePlayingController {

    private RolePlayingService rolePlayingservice;

    public RolePlayingController(RolePlayingService rolePlayingservice) {
        this.rolePlayingservice = rolePlayingservice;
    }

    @GetMapping("")
    public Iterable<RolePlaying> getRolePlaying() {
        return rolePlayingservice.getRolePlaying();
    }

    @GetMapping("/{id}")
    public Optional<RolePlaying> getRolePlaying(@PathVariable Long id) {
        return rolePlayingservice.getOneRolePlaying(id);
    }

    @DeleteMapping("/{id}")
    public void deleteRolePlayingByID(@PathVariable Long id) {
        rolePlayingservice.deleteRolePlayingByID(id);
    }

    @PostMapping("")
    private RolePlaying saveRolePlaying(@RequestBody RolePlaying rolePlaying) {
        return rolePlayingservice.saveRolePlaying(rolePlaying);
    }

    @PutMapping("/{id}")
    private RolePlaying updateRolePlaying(@PathVariable Long id, @RequestBody RolePlaying rolePlaying) {
        return rolePlayingservice.updateRolePlaying(id, rolePlaying);
    }
}
