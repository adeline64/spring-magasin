package com.mag.magasin.controller;

import com.mag.magasin.entity.BoardGames;
import com.mag.magasin.service.BoardGameService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/product/boardGame")
@RestController
public class BoardGameController {

    private BoardGameService boardGamesservice;

    public BoardGameController(BoardGameService boardGamesservice) {
        this.boardGamesservice = boardGamesservice;
    }

    @GetMapping("")
    public Iterable<BoardGames> getBoardGames() {
        return boardGamesservice.getBoardGames();
    }

    @GetMapping("/{id}")
    public Optional<BoardGames> getBoardGames(@PathVariable Long id) {
        return boardGamesservice.getOneBoardGames(id);
    }

    @DeleteMapping("/{id}")
    public void deleteBoardGamesByID(@PathVariable Long id) {
        boardGamesservice.deleteBoardGamesByID(id);
    }

    @PostMapping("")
    private BoardGames saveBoardGames(@RequestBody BoardGames boardGames) {
        return boardGamesservice.saveBoardGames(boardGames);
    }

    @PutMapping("/{id}")
    private BoardGames updateBoardGames(@PathVariable Long id, @RequestBody BoardGames boardGames) {
        System.out.println("passe");
        return boardGamesservice.updateBoardGames(id, boardGames);
    }
}
