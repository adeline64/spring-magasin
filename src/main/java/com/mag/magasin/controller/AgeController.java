package com.mag.magasin.controller;

import com.mag.magasin.entity.Age;
import com.mag.magasin.service.AgeService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/ages")
@RestController
public class AgeController {

    private AgeService ageservice;

    public AgeController(AgeService ageservice) {
        this.ageservice = ageservice;
    }

    @GetMapping("")
    public Iterable<Age> getAge() {
        return ageservice.getAge();
    }

    @GetMapping("/{id}")
    public Optional<Age> getAge(@PathVariable Long id) {
        return ageservice.getOneAge(id);
    }

    @DeleteMapping("/{id}")
    public void deleteAgeByID(@PathVariable Long id) {
        ageservice.deleteAgeByID(id);
    }

    @PostMapping("")
    private Age saveAge(@RequestBody Age age) {
        return ageservice.save(age);
    }

    @PutMapping("/{id}")
    private Age updateAge(@PathVariable Long id, @RequestBody Age age) {
        return ageservice.updateAge(id, age);
    }
}
