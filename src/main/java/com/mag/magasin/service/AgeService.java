package com.mag.magasin.service;

import com.mag.magasin.entity.Age;

import java.util.Optional;


public interface AgeService {
    Iterable<Age> getAge();

    Optional<Age> getOneAge(Long id);

    void deleteAgeByID(Long id);

    Age save(Age age);

    Age updateAge(Long id, Age age);

}
