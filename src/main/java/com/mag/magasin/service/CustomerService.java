package com.mag.magasin.service;

import com.mag.magasin.entity.Customer;
import java.util.Optional;


public interface CustomerService{
    Iterable<Customer> getCustomer();

    Optional<Customer> getOneCustomer(Long id);

    void deleteCustomerByID(Long id);

    Customer save(Customer customer);

    Customer updateCustomer(Long id, Customer customer);

}
