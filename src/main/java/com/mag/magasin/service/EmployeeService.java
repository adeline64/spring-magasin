package com.mag.magasin.service;

import com.mag.magasin.entity.Employee;

import java.util.Optional;

public interface EmployeeService{
    Iterable<Employee> getEmployee();

    Optional<Employee> getOneEmployee(Long id);

    void deleteEmployeeByID(Long id);

    Employee save(Employee employee);

    Employee updateEmployee(Long id, Employee employee);

}
