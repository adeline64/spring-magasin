package com.mag.magasin.service.implement;

import com.mag.magasin.entity.Store;
import com.mag.magasin.repository.StoreRepository;
import com.mag.magasin.service.StoreService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StoreServiceImpl implements StoreService {

    private StoreRepository storeRepository;

    public StoreServiceImpl(StoreRepository storeRepository) {
        this.storeRepository = storeRepository;
    }

    @Override
    public Iterable<Store> getStore() {
        return storeRepository.findAll();
    }

    @Override
    public Optional<Store> getOneStore(Long id) {
        return storeRepository.findById(id);
    }

    @Override
    public void deleteStoreByID(Long id) {
        storeRepository.deleteById(id);
    }

    @Override
    public Store save(Store store) {
        return storeRepository.save(store);
    }

    @Override
    public Store updateStore(Long id, Store store) {
        var a = storeRepository.findById(id);
        if (a.isPresent()) {
            var courentStore = a.get();
            courentStore.setSiret(store.getSiret());
            courentStore.setSiren(store.getSiren());
            courentStore.setNumberOfEmployees(store.getNumberOfEmployees());
            courentStore.setEmail(store.getEmail());
            courentStore.setAddress(store.getAddress());
            courentStore.setProduct(store.getProduct());
            courentStore.setEmployees(store.getEmployees());
            courentStore.setCustomers(store.getCustomers());
            return storeRepository.save(courentStore);
        } else {
            return null;
        }
    }
}
