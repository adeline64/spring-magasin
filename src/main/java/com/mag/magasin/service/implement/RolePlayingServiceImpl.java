package com.mag.magasin.service.implement;

import com.mag.magasin.entity.RolePlaying;
import com.mag.magasin.repository.RolePlayingRepository;
import com.mag.magasin.service.RolePlayingService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RolePlayingServiceImpl implements RolePlayingService {

    RolePlayingRepository rolePlayingRepository;

    public RolePlayingServiceImpl(RolePlayingRepository rolePlayingRepository) {
        this.rolePlayingRepository = rolePlayingRepository;
    }

    @Override
    public Iterable<RolePlaying> getRolePlaying() {
        return rolePlayingRepository.findAll();
    }

    @Override
    public Optional<RolePlaying> getOneRolePlaying(Long id) {
        return rolePlayingRepository.findById(id);
    }

    @Override
    public void deleteRolePlayingByID(Long id) {
        rolePlayingRepository.deleteById(id);
    }

    @Override
    public RolePlaying saveRolePlaying(RolePlaying rolePlaying) {
        return rolePlayingRepository.save(rolePlaying);
    }

    @Override
    public RolePlaying updateRolePlaying(Long id, RolePlaying rolePlaying) {
        var cg = rolePlayingRepository.findById(id);
        if (cg.isPresent()) {
            var courentRolePlaying = cg.get();
            courentRolePlaying.setBrand(rolePlaying.getBrand());
            courentRolePlaying.setBarCode(rolePlaying.getBarCode());
            courentRolePlaying.setPrice(rolePlaying.getPrice());
            courentRolePlaying.setStock(rolePlaying.getStock());
            courentRolePlaying.setStore(rolePlaying.getStore());
            courentRolePlaying.setAge(rolePlaying.getAge());
            courentRolePlaying.setReservation(rolePlaying.getReservation());
            courentRolePlaying.setNumberOfPages(rolePlaying.getNumberOfPages());
            return rolePlayingRepository.save(courentRolePlaying);
        } else {
            return null;
        }
    }
}
