package com.mag.magasin.service.implement;

import com.mag.magasin.entity.Age;
import com.mag.magasin.repository.AgeRepository;
import com.mag.magasin.service.AgeService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AgeServiceImpl implements AgeService {

    private AgeRepository ageRepository;

    public AgeServiceImpl(AgeRepository ageRepository) {
        this.ageRepository = ageRepository;
    }

    @Override
    public Iterable<Age> getAge() {
        return ageRepository.findAll();
    }

    @Override
    public Optional<Age> getOneAge(Long id) {
        return ageRepository.findById(id);
    }

    @Override
    public void deleteAgeByID(Long id) {
        ageRepository.deleteById(id);
    }

    @Override
    public Age save(Age age) {
        return ageRepository.save(age);
    }

    @Override
    public Age updateAge(Long id, Age age) {
        var ag = ageRepository.findById(id);
        if (ag.isPresent()) {
            var courentAge = ag.get();
            courentAge.setMaximumAge(age.getMaximumAge());
            courentAge.setMinimumAge(age.getMinimumAge());
            courentAge.setProduct(age.getProduct());
            return ageRepository.save(courentAge);
        } else {
            return null;
        }
    }
}
