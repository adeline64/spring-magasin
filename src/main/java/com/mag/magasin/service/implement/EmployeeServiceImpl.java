package com.mag.magasin.service.implement;

import com.mag.magasin.entity.Employee;
import com.mag.magasin.repository.EmployeeRepository;
import com.mag.magasin.service.EmployeeService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Iterable<Employee> getEmployee() {
        return employeeRepository.findAll();
    }

    @Override
    public Optional<Employee> getOneEmployee(Long id) {
        return employeeRepository.findById(id);
    }

    @Override
    public void deleteEmployeeByID(Long id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public Employee updateEmployee(Long id, Employee employee) {
        var e = employeeRepository.findById(id);
        if (e.isPresent()) {
            var courentEmployee = e.get();
            courentEmployee.setPoste(employee.getPoste());
            courentEmployee.setPassword(employee.getPassword());
            courentEmployee.setGrade(employee.getGrade());
            courentEmployee.setEmail(employee.getEmail());
            courentEmployee.setDateOfBirth(employee.getDateOfBirth());
            courentEmployee.setFirstName(employee.getFirstName());
            courentEmployee.setLastName(employee.getLastName());
            courentEmployee.setTelephone(employee.getTelephone());
            return employeeRepository.save(courentEmployee);
        } else {
            return null;
        }
    }
}
