package com.mag.magasin.service.implement;

import com.mag.magasin.entity.Customer;
import com.mag.magasin.repository.CustomerReposiory;
import com.mag.magasin.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    CustomerReposiory customerReposiory;

    public CustomerServiceImpl(CustomerReposiory customerReposiory) {
        this.customerReposiory = customerReposiory;
    }

    @Override
    public Iterable<Customer> getCustomer() {
        return customerReposiory.findAll();
    }

    @Override
    public Optional<Customer> getOneCustomer(Long id) {
        return customerReposiory.findById(id);
    }

    @Override
    public void deleteCustomerByID(Long id) {
        customerReposiory.deleteById(id);
    }

    @Override
    public Customer save(Customer customer) {
        return customerReposiory.save(customer);
    }

    @Override
    public Customer updateCustomer(Long id, Customer customer) {
        var c = customerReposiory.findById(id);
        if (c.isPresent()) {
            var courentCustumer = c.get();
            courentCustumer.setEmail(customer.getEmail());
            courentCustumer.setDateOfBirth(customer.getDateOfBirth());
            courentCustumer.setFirstName(customer.getFirstName());
            courentCustumer.setLastName(customer.getLastName());
            courentCustumer.setTelephone(customer.getTelephone());
            return customerReposiory.save(courentCustumer);
        } else {
            return null;
        }
    }
}
