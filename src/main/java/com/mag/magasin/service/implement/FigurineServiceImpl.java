package com.mag.magasin.service.implement;

import com.mag.magasin.entity.Figurine;
import com.mag.magasin.repository.FigurineRepository;
import com.mag.magasin.service.FigurineService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FigurineServiceImpl implements FigurineService {

    FigurineRepository figurineRepository;

    public FigurineServiceImpl(FigurineRepository figurineRepository) {
        this.figurineRepository = figurineRepository;
    }

    @Override
    public Iterable<Figurine> getFigurine() {
        return figurineRepository.findAll();
    }

    @Override
    public Optional<Figurine> getOneFigurine(Long id) {
        return figurineRepository.findById(id);
    }

    @Override
    public void deleteFigurineByID(Long id) {
        figurineRepository.deleteById(id);
    }

    @Override
    public Figurine save(Figurine figurine) {
        return figurineRepository.save(figurine);
    }

    @Override
    public Figurine updateFigurine(Long id, Figurine figurine) {
        var a = figurineRepository.findById(id);
        if (a.isPresent()) {
            var courentFigurine = a.get();
            courentFigurine.setBrand(figurine.getBrand());
            courentFigurine.setBarCode(figurine.getBarCode());
            courentFigurine.setPrice(figurine.getPrice());
            courentFigurine.setStock(figurine.getStock());
            courentFigurine.setStore(figurine.getStore());
            courentFigurine.setAge(figurine.getAge());
            courentFigurine.setReservation(figurine.getReservation());
            courentFigurine.setDepth(figurine.getDepth());
            courentFigurine.setHeight(figurine.getHeight());
            courentFigurine.setLenght(figurine.getLenght());
            return figurineRepository.save(courentFigurine);
        } else {
            return null;
        }
    }
}
