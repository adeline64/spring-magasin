package com.mag.magasin.service.implement;

import com.mag.magasin.entity.CardGames;
import com.mag.magasin.repository.CardGamesRepository;
import com.mag.magasin.service.CardGamesservice;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CardGamesServiceImpl implements CardGamesservice {

    private CardGamesRepository cardGamesRepository;

    public CardGamesServiceImpl(CardGamesRepository cardGamesRepository) {
        this.cardGamesRepository = cardGamesRepository;
    }

    @Override
    public Iterable<CardGames> getCardGames() {
        return cardGamesRepository.findAll();
    }

    @Override
    public Optional<CardGames> getOneCardGames(Long id) {
        return cardGamesRepository.findById(id);
    }

    @Override
    public void deleteCardGamesByID(Long id) {
        cardGamesRepository.deleteById(id);
    }

    @Override
    public CardGames saveCardGames(CardGames cardGames) {
        return cardGamesRepository.save(cardGames);
    }

    @Override
    public CardGames updateCardGames(Long id, CardGames cardGames) {
        var cg = cardGamesRepository.findById(id);
        if (cg.isPresent()) {
            var courentCarddGame = cg.get();
            courentCarddGame.setBrand(cardGames.getBrand());
            courentCarddGame.setBarCode(cardGames.getBarCode());
            courentCarddGame.setPrice(cardGames.getPrice());
            courentCarddGame.setStock(cardGames.getStock());
            courentCarddGame.setStore(cardGames.getStore());
            courentCarddGame.setAge(cardGames.getAge());
            courentCarddGame.setReservation(cardGames.getReservation());
            courentCarddGame.setCardNumbers(cardGames.getCardNumbers());
            return cardGamesRepository.save(courentCarddGame);
        } else {
            return null;
        }
    }
}
