package com.mag.magasin.service.implement;

import com.mag.magasin.entity.Reservation;
import com.mag.magasin.repository.ReservationRepository;
import com.mag.magasin.service.ReservationService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ReservationServiceImpl implements ReservationService {

    ReservationRepository reservationRepository;

    public ReservationServiceImpl(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @Override
    public Iterable<Reservation> getReservation() {
        return reservationRepository.findAll();
    }

    @Override
    public Optional<Reservation> getOneReservation(Long id) {
        return reservationRepository.findById(id);
    }

    @Override
    public void deleteReservationByID(Long id) {
        reservationRepository.deleteById(id);
    }

    @Override
    public Reservation saveReservation(Reservation reservation) {
        return reservationRepository.save(reservation);
    }

    @Override
    public Reservation updateReservation(Long id, Reservation reservation) {
        var bg = reservationRepository.findById(id);
        if (bg.isPresent()) {
            var courentReservation = bg.get();
            courentReservation.setProduct(reservation.getProduct());
            courentReservation.setStatus(reservation.getStatus());
            courentReservation.setStore(reservation.getStore());
            return reservationRepository.save(courentReservation);
        } else {
            return null;
        }
    }
}
