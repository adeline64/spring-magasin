package com.mag.magasin.service.implement;

import com.mag.magasin.entity.BoardGames;
import com.mag.magasin.repository.BoardGameRepository;
import com.mag.magasin.service.BoardGameService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BoardGameServiceImpl implements BoardGameService {

    private BoardGameRepository boardGameRepository;

    public BoardGameServiceImpl(BoardGameRepository boardGameRepository) {
        this.boardGameRepository = boardGameRepository;
    }

    @Override
    public Iterable<BoardGames> getBoardGames() {
        return boardGameRepository.findAll();
    }

    @Override
    public Optional<BoardGames> getOneBoardGames(Long id) {
        return boardGameRepository.findById(id);
    }

    @Override
    public void deleteBoardGamesByID(Long id) {
        boardGameRepository.deleteById(id);
    }

    @Override
    public BoardGames saveBoardGames(BoardGames boardGames) {
        return boardGameRepository.save(boardGames);
    }

    @Override
    public BoardGames updateBoardGames(Long id, BoardGames boardGames) {
        var bg = boardGameRepository.findById(id);
        if (bg.isPresent()) {
            var courentboardGame = bg.get();
            courentboardGame.setBrand(boardGames.getBrand());
            courentboardGame.setBarCode(boardGames.getBarCode());
            courentboardGame.setPrice(boardGames.getPrice());
            courentboardGame.setStock(boardGames.getStock());
            courentboardGame.setStore(boardGames.getStore());
            courentboardGame.setAge(boardGames.getAge());
            courentboardGame.setReservation(boardGames.getReservation());
            courentboardGame.setNumberMax(boardGames.getNumberMax());
            courentboardGame.setNumberMin(boardGames.getNumberMin());
            return boardGameRepository.save(courentboardGame);
        } else {
            return null;
        }
    }
}
