package com.mag.magasin.service.implement;

import com.mag.magasin.entity.Address;
import com.mag.magasin.repository.AddressRepository;
import com.mag.magasin.service.AddressService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {

    private AddressRepository addressRepository;

    public AddressServiceImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public Iterable<Address> getAddress() {
        return addressRepository.findAll();
    }

    @Override
    public Optional<Address> getOneAddress(Long id) {
        return addressRepository.findById(id);
    }

    @Override
    public void deleteAddressByID(Long id) {
        addressRepository.deleteById(id);
    }

    @Override
    public Address save(Address address) {
        return addressRepository.save(address);
    }

    @Override
    public Address updateAddress(Long id, Address address) {
        var a = addressRepository.findById(id);
        if (a.isPresent()) {
            var courentAddress = a.get();
            courentAddress.setAddress(address.getAddress());
            courentAddress.setTown(address.getTown());
            courentAddress.setPostalCode(address.getPostalCode());
            courentAddress.setUser(address.getUser());
            return addressRepository.save(courentAddress);
        } else {
            return null;
        }
    }
}
