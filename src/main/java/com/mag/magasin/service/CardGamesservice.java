package com.mag.magasin.service;


import com.mag.magasin.entity.CardGames;

import java.util.Optional;

public interface CardGamesservice {
    Iterable<CardGames> getCardGames();

    Optional<CardGames> getOneCardGames(Long id);

    void deleteCardGamesByID(Long id);

    CardGames saveCardGames(CardGames cardGames);

    CardGames updateCardGames(Long id, CardGames cardGames);

}
