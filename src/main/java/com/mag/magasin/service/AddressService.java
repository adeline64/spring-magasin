package com.mag.magasin.service;

import com.mag.magasin.entity.Address;

import java.util.Optional;

public interface AddressService {
    Iterable<Address> getAddress();

    Optional<Address> getOneAddress(Long id);

    void deleteAddressByID(Long id);

    Address save(Address address);

    Address updateAddress(Long id, Address address);

}
