package com.mag.magasin.service;

import com.mag.magasin.entity.Reservation;

import java.util.Optional;


public interface ReservationService {
    Iterable<Reservation> getReservation();

    Optional<Reservation> getOneReservation(Long id);

    void deleteReservationByID(Long id);

    Reservation saveReservation(Reservation reservation);

    Reservation updateReservation(Long id, Reservation reservation);

}
