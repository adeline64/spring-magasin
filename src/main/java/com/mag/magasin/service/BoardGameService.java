package com.mag.magasin.service;

import com.mag.magasin.entity.BoardGames;

import java.util.Optional;


public interface BoardGameService {
    Iterable<BoardGames> getBoardGames();

    Optional<BoardGames> getOneBoardGames(Long id);

    void deleteBoardGamesByID(Long id);

    BoardGames saveBoardGames(BoardGames boardGames);

    BoardGames updateBoardGames(Long id, BoardGames boardGames);

}
