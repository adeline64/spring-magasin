package com.mag.magasin.service;

import com.mag.magasin.entity.Figurine;

import java.util.Optional;

public interface FigurineService {
    Iterable<Figurine> getFigurine();

    Optional<Figurine> getOneFigurine(Long id);

    void deleteFigurineByID(Long id);

    Figurine save(Figurine figurine);

    Figurine updateFigurine(Long id, Figurine figurine);

}
