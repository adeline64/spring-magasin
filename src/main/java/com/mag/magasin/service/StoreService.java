package com.mag.magasin.service;

import com.mag.magasin.entity.Store;

import java.util.Optional;

public interface StoreService {
    Iterable<Store> getStore();

    Optional<Store> getOneStore(Long id);

    void deleteStoreByID(Long id);

    Store save(Store store);

    Store updateStore(Long id, Store store);

}
