package com.mag.magasin.service;

import com.mag.magasin.entity.RolePlaying;

import java.util.Optional;

public interface RolePlayingService {
    Iterable<RolePlaying> getRolePlaying();

    Optional<RolePlaying> getOneRolePlaying(Long id);

    void deleteRolePlayingByID(Long id);

    RolePlaying saveRolePlaying(RolePlaying rolePlaying);

    RolePlaying updateRolePlaying(Long id, RolePlaying rolePlaying);

}
