package com.mag.magasin.repository;

import com.mag.magasin.entity.RolePlaying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolePlayingRepository extends CrudRepository<RolePlaying, Long> {
}
