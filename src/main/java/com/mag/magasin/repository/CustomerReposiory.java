package com.mag.magasin.repository;

import com.mag.magasin.entity.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerReposiory extends CrudRepository<Customer, Long> {
}
