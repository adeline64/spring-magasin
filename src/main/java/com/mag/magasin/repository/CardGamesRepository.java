package com.mag.magasin.repository;

import com.mag.magasin.entity.CardGames;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardGamesRepository extends CrudRepository<CardGames, Long> {
}
