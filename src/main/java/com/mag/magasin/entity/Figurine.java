package com.mag.magasin.entity;

import com.sun.istack.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "figurine")
public class Figurine extends Product{

    @Column(name = "height", nullable = false)
    @NotNull
    private float height;

    @Column(name = "lenght", nullable = false)
    @NotNull
    private float lenght;

    @Column(name = "depth", nullable = false)
    @NotNull
    private float depth;

    public Figurine() {
    }

    public Figurine(Long id, String brand, int barCode, float price, int stock, List<Store> store, List<Age> age, List<Reservation> reservation, float height, float lenght, float depth) {
        super(id, brand, barCode, price, stock, store, age, reservation);
        this.height = height;
        this.lenght = lenght;
        this.depth = depth;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getLenght() {
        return lenght;
    }

    public void setLenght(float lenght) {
        this.lenght = lenght;
    }

    public float getDepth() {
        return depth;
    }

    public void setDepth(float depth) {
        this.depth = depth;
    }

    @Override
    public String toString() {
        return "Figurine{" +
                "height=" + height +
                ", lenght=" + lenght +
                ", depth=" + depth +
                '}';
    }
}
