package com.mag.magasin.entity;

import javax.persistence.*;
import com.sun.istack.NotNull;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "store")
public class Store {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, name = "siret")
    @NotNull
    private String siret;
    @Column(nullable = false, name = "siren")
    @NotNull
    private String siren;
    @Column(nullable = false, name = "number_of_employees")
    @NotNull
    private String numberOfEmployees;
    @Column(nullable = false, name = "email", unique = true)
    @NotNull
    private String email;
    @OneToOne
    @JoinColumn(name = "id_address", referencedColumnName = "id")
    private Address address;
    @ManyToMany
    @JoinTable(name = "store_product",
            joinColumns = @JoinColumn(name = "store_id",  referencedColumnName="ID"),
            inverseJoinColumns = @JoinColumn(name = "product_id",  referencedColumnName="ID")
    )
    private List<Product> product = new ArrayList<>();
    @ManyToMany
    @JoinTable(name = "employee_store",
            joinColumns = @JoinColumn(name = "store_id",  referencedColumnName="ID"),
            inverseJoinColumns = @JoinColumn(name = "employee_id",  referencedColumnName="ID")
    )
    private List<Employee> employees = new ArrayList<>();
    @ManyToMany
    @JoinTable(name = "customer_store",
            joinColumns = @JoinColumn(name = "store_id",  referencedColumnName="ID"),
            inverseJoinColumns = @JoinColumn(name = "customer_id",  referencedColumnName="ID")
    )
    private List<Customer> customers = new ArrayList<>();

    public Store() {
    }

    public Store(Long id, String siret, String siren, String numberOfEmployees, String email, Address address, List<Product> product, List<Employee> employees, List<Customer> customers) {
        this.id = id;
        this.siret = siret;
        this.siren = siren;
        this.numberOfEmployees = numberOfEmployees;
        this.email = email;
        this.address = address;
        this.product = product;
        this.employees = employees;
        this.customers = customers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public String getSiren() {
        return siren;
    }

    public void setSiren(String siren) {
        this.siren = siren;
    }

    public String getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(String numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    @Override
    public String toString() {
        return "Store{" +
                "id=" + id +
                ", siret='" + siret + '\'' +
                ", siren='" + siren + '\'' +
                ", numberOfEmployees='" + numberOfEmployees + '\'' +
                ", email='" + email + '\'' +
                ", address=" + address +
                ", product=" + product +
                ", employees=" + employees +
                ", customers=" + customers +
                '}';
    }
}
