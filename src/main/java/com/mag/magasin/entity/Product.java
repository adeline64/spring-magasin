package com.mag.magasin.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "product")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, name = "brand")
    @NotNull
    private String brand;

    @Column(nullable = false, name = "bar_code")
    @NotNull
    private int barCode;

    @Column(nullable = false, name = "price")
    @NotNull
    private float price;

    @Column(nullable = false, name = "stock")
    @NotNull
    private int stock;

    @ManyToMany(mappedBy = "product")
    private List<Store> store = new ArrayList<>();

    @ManyToMany(mappedBy = "product")
    private List<Age> age = new ArrayList<>();

    @ManyToMany(mappedBy = "product")
    private List<Reservation> reservation = new ArrayList<>();


    public Product() {
    }

    public Product(Long id, String brand, int barCode, float price, int stock, List<Store> store, List<Age> age, List<Reservation> reservation) {
        this.id = id;
        this.brand = brand;
        this.barCode = barCode;
        this.price = price;
        this.stock = stock;
        this.store = store;
        this.age = age;
        this.reservation = reservation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getBarCode() {
        return barCode;
    }

    public void setBarCode(int barCode) {
        this.barCode = barCode;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public List<Store> getStore() {
        return store;
    }

    public void setStore(List<Store> store) {
        this.store = store;
    }

    public List<Age> getAge() {
        return age;
    }

    public void setAge(List<Age> age) {
        this.age = age;
    }

    public List<Reservation> getReservation() {
        return reservation;
    }

    public void setReservation(List<Reservation> reservation) {
        this.reservation = reservation;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", barCode=" + barCode +
                ", price=" + price +
                ", stock=" + stock +
                ", store=" + store +
                ", age=" + age +
                ", reservation=" + reservation +
                '}';
    }
}
