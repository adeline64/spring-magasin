package com.mag.magasin.entity;

import com.sun.istack.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "boardGames")
public class BoardGames extends Product{

    @Column(name = "number_min", nullable = false)
    @NotNull
    private Integer numberMin;

    @Column(name = "number_max", nullable = false)
    @NotNull
    private Integer numberMax;

    public BoardGames() {
    }

    public BoardGames(Long id, String brand, int barCode, float price, int stock, List<Store> store, List<Age> age, List<Reservation> reservation, Integer numberMin, Integer numberMax) {
        super(id, brand, barCode, price, stock, store, age, reservation);
        this.numberMin = numberMin;
        this.numberMax = numberMax;
    }

    public Integer getNumberMin() {
        return numberMin;
    }

    public void setNumberMin(Integer numberMin) {
        this.numberMin = numberMin;
    }

    public Integer getNumberMax() {
        return numberMax;
    }

    public void setNumberMax(Integer numberMax) {
        this.numberMax = numberMax;
    }

    @Override
    public String toString() {
        return "BoardGames{" +
                "numberMin=" + numberMin +
                ", numberMax=" + numberMax +
                '}';
    }
}
