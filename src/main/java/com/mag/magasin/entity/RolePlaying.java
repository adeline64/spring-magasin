package com.mag.magasin.entity;

import com.sun.istack.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "rolePlaying")
public class RolePlaying extends Product{

    @Column(name = "number_of_pages", nullable = false)
    @NotNull
    private Integer numberOfPages;

    public RolePlaying() {
    }

    public RolePlaying(Long id, String brand, int barCode, float price, int stock, List<Store> store, List<Age> age, List<Reservation> reservation, Integer numberOfPages) {
        super(id, brand, barCode, price, stock, store, age, reservation);
        this.numberOfPages = numberOfPages;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    @Override
    public String toString() {
        return "RolePlaying{" +
                "numberOfPages=" + numberOfPages +
                '}';
    }
}
