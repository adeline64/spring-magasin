package com.mag.magasin.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "reservation")
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private ReservationStatus status;
    public enum ReservationStatus {
        InProgess,
        Bought,
        canceled;
    }
    @OneToOne
    @JoinColumn(name = "id_store")
    private Store store;
    @ManyToMany
    @JoinTable(name = "reservation_product",
            joinColumns = @JoinColumn(name = "reservation_id",  referencedColumnName="ID"),
            inverseJoinColumns = @JoinColumn(name = "product_id",  referencedColumnName="ID")
    )
    private List<Product> product = new ArrayList<>();

    public Reservation() {
    }

    public Reservation(Long id, ReservationStatus status, Store store, List<Product> product) {
        this.id = id;
        this.status = status;
        this.store = store;
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ReservationStatus getStatus() {
        return status;
    }

    public void setStatus(ReservationStatus status) {
        this.status = status;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", status=" + status +
                ", store=" + store +
                ", product=" + product +
                '}';
    }
}
