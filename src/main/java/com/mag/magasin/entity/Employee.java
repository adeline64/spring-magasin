package com.mag.magasin.entity;

import com.sun.istack.NotNull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "employee")
public class Employee extends User{

    @Column(name = "poste", nullable = false)
    @NotNull
    private String poste;
    @Column(name = "grade", nullable = false)
    @NotNull
    private String grade;
    @Column(name = "num_vendeur", nullable = false)
    @NotNull
    private String numVendeur;
    @Column(name = "password", nullable = false)
    @NotNull
    private String password;
    @ManyToMany(mappedBy = "employees")
    private List<Store> stores = new ArrayList<>();

    public Employee() {
    }

    public Employee(Long id, String lastName, String telephone, Date dateOfBirth, String firstName, String email, List<Address> address, String poste, String grade, String numVendeur, String password, List<Store> stores) {
        super(id, lastName, telephone, dateOfBirth, firstName, email, address);
        this.poste = poste;
        this.grade = grade;
        this.numVendeur = numVendeur;
        this.password = password;
        this.stores = stores;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getNumVendeur() {
        return numVendeur;
    }

    public void setNumVendeur(String numVendeur) {
        this.numVendeur = numVendeur;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Store> getStores() {
        return stores;
    }

    public void setStores(List<Store> stores) {
        this.stores = stores;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "poste='" + poste + '\'' +
                ", grade='" + grade + '\'' +
                ", numVendeur='" + numVendeur + '\'' +
                ", password='" + password + '\'' +
                ", stores=" + stores +
                '}';
    }
}
