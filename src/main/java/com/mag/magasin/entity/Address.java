package com.mag.magasin.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.Id;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, name = "town")
    @NotNull
    private String town;
    @Column(nullable = false, name = "postal_code")
    @NotNull
    private String postalCode;
    @Column(nullable = false, name = "address")
    @NotNull
    private String address;
    @ManyToMany
    @JoinTable(name = "user_address",
            joinColumns = @JoinColumn(name = "address_id",  referencedColumnName="ID"),
            inverseJoinColumns = @JoinColumn(name = "user_id",  referencedColumnName="ID")
    )
    private List<User> user = new ArrayList<>();

    public Address() {
    }

    public Address(Long id, String town, String postalCode, String address, List<User> user) {
        this.id = id;
        this.town = town;
        this.postalCode = postalCode;
        this.address = address;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", town='" + town + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", address='" + address + '\'' +
                ", user=" + user +
                '}';
    }
}
