package com.mag.magasin.entity;

import com.sun.istack.NotNull;
import javax.persistence.*;
import java.util.Date;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false, name = "last_name")
    @NotNull
    private String lastName;
    @Column(nullable = false, name = "telephone")
    @NotNull
    private String telephone;
    @Column(nullable = false, name = "date_of_birth")
    @NotNull
    private Date dateOfBirth;
    @Column(nullable = false, name = "first_name")
    @NotNull
    private String firstName;
    @Column(nullable = false, name = "email", unique = true)
    @NotNull
    private String email;
    @ManyToMany(mappedBy = "user")
    private List<Address> address = new ArrayList<>();


    public User() {
    }

    public User(Long id, String lastName, String telephone, Date dateOfBirth, String firstName, String email, List<Address> address) {
        this.id = id;
        this.lastName = lastName;
        this.telephone = telephone;
        this.dateOfBirth = dateOfBirth;
        this.firstName = firstName;
        this.email = email;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", telephone='" + telephone + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", firstName='" + firstName + '\'' +
                ", email='" + email + '\'' +
                ", address=" + address +
                '}';
    }
}
