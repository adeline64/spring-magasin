package com.mag.magasin.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Table(name = "customer")
@Entity
public class Customer extends User {

    @Column(nullable = false, name = "num_client")
    @NotNull
    private String numClient;
    @ManyToMany(mappedBy = "customers")
    private List<Store> stores = new ArrayList<>();

    public Customer() {
    }

    public Customer(Long id, String lastName, String telephone, Date dateOfBirth, String firstName, String email, List<Address> address, String numClient, List<Store> stores) {
        super(id, lastName, telephone, dateOfBirth, firstName, email, address);
        this.numClient = numClient;
        this.stores = stores;
    }

    public String getNumClient() {
        return numClient;
    }

    public void setNumClient(String numClient) {
        this.numClient = numClient;
    }

    public List<Store> getStores() {
        return stores;
    }

    public void setStores(List<Store> stores) {
        this.stores = stores;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "numClient='" + numClient + '\'' +
                ", stores=" + stores +
                '}';
    }
}
