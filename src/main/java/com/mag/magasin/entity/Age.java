package com.mag.magasin.entity;

import javax.persistence.*;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "age")
public class Age {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "minimum_age")
    private int minimumAge;
    @Column(name = "maximum_age")
    private int maximumAge;
    @ManyToMany
        @JoinTable(name = "product_age",
        joinColumns = @JoinColumn(name = "age_id",  referencedColumnName="ID"),
        inverseJoinColumns = @JoinColumn(name = "product_id",  referencedColumnName="ID")
    )
    private List<Product> product = new ArrayList<>();

    public Age() {
    }

    public Age(Long id, int minimumAge, int maximumAge, List<Product> product) {
        this.id = id;
        this.minimumAge = minimumAge;
        this.maximumAge = maximumAge;
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getMinimumAge() {
        return minimumAge;
    }

    public void setMinimumAge(int minimumAge) {
        this.minimumAge = minimumAge;
    }

    public int getMaximumAge() {
        return maximumAge;
    }

    public void setMaximumAge(int maximumAge) {
        this.maximumAge = maximumAge;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "Age{" +
                "id=" + id +
                ", minimumAge=" + minimumAge +
                ", maximumAge=" + maximumAge +
                ", product=" + product +
                '}';
    }
}
