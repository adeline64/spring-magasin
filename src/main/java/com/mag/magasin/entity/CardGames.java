package com.mag.magasin.entity;

import com.sun.istack.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "cardGames")
public class CardGames extends Product{

    @Column(name = "card_numbers", nullable = false)
    @NotNull
    private Integer cardNumbers;

    public CardGames() {
    }

    public CardGames(Long id, String brand, int barCode, float price, int stock, List<Store> store, List<Age> age, List<Reservation> reservation, Integer cardNumbers) {
        super(id, brand, barCode, price, stock, store, age, reservation);
        this.cardNumbers = cardNumbers;
    }

    public Integer getCardNumbers() {
        return cardNumbers;
    }

    public void setCardNumbers(Integer cardNumbers) {
        this.cardNumbers = cardNumbers;
    }

    @Override
    public String toString() {
        return "CardGames{" +
                "cardNumbers=" + cardNumbers +
                '}';
    }
}
